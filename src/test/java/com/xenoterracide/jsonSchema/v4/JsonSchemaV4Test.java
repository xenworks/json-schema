package com.xenoterracide.jsonSchema.v4;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import static com.jayway.jsonpath.matchers.JsonPathMatchers.hasJsonPath;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;


class JsonSchemaV4Test {

    private final JsonSchemaV4 serializer;
    private final ObjectMapper mapper;

    JsonSchemaV4Test() {
        this.mapper = new ObjectMapper();
        this.serializer = new JsonSchemaV4( mapper );
    }

    @Test
    void serialize() {
        String json = serializer.apply( User.class ).toString();


        assertThat( json, hasJsonPath( "$.title", is( User.class.getSimpleName() ) ) );
        assertThat( json, hasJsonPath( "$.type", is( "object" ) ) );
        assertThat( json, hasJsonPath( "$.properties.email.title", is( "Email" ) ) );
        assertThat( json, hasJsonPath( "$.properties.email.readOnly", is( false ) ) );
        assertThat( json, hasJsonPath( "$.properties.email.type", is( "string" ) ) );
        assertThat( json, hasJsonPath( "$.$schema", is( "http://json-schema.org/draft-04/schema#" ) ) );
        assertThat( json, hasJsonPath( "$.definitions" ) );
    }

    static class User {
        private String email;

        public String getEmail() {
            return email;
        }

        public void setEmail( String email ) {
            this.email = email;
        }
    }
}
