package com.xenoterracide.jsonSchema.v4;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeType;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.core.annotation.AnnotatedElementUtils;
import org.springframework.util.ReflectionUtils;
import org.springframework.util.StringUtils;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Locale;
import java.util.Objects;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Stream;

public class JsonSchemaV4 implements Function<Class<?>, ObjectNode> {
    private final ObjectMapper mapper;

    public JsonSchemaV4( ObjectMapper mapper ) {
        this.mapper = Objects.requireNonNull( mapper );
    }

    private static boolean filter( Field field ) {
        return !AnnotatedElementUtils.isAnnotated( field, JsonIgnore.class )
                && !Modifier.isStatic( field.getModifiers() );
    }

    @Override
    public ObjectNode apply( Class<?> aClass ) {
        ObjectNode properties = mapper.createObjectNode();
        ReflectionUtils.doWithFields( aClass, field -> {
            ObjectNode prop = mapper.createObjectNode();
            prop.put( "title", StringUtils.capitalize( field.getName() ) );
            prop.put( "readOnly", false );
            prop.put( "type", classToType( field.getType() ).toString().toLowerCase( Locale.getDefault() ) );
            properties.set( field.getName(), prop );
        }, JsonSchemaV4::filter );
        ObjectNode root = mapper.createObjectNode();
        root.put( "title", aClass.getSimpleName() );
        root.put( "type", root.getNodeType().toString().toLowerCase( Locale.getDefault() ) );
        root.put( "$schema", "http://json-schema.org/draft-04/schema#" );
        root.set( "definitions", mapper.createArrayNode() );
        root.set( "properties", properties );
        return root;
    }

    private static JsonNodeType classToType( Class<?> clazz ) {
        Objects.requireNonNull( clazz );
        if ( Stream.of( CharSequence.class, UUID.class ).anyMatch( c -> c.isAssignableFrom( clazz ) ) ) {
            return JsonNodeType.STRING;
        }
        if ( Iterable.class.isAssignableFrom( clazz ) ) {
            return JsonNodeType.ARRAY;
        }
        if ( Number.class.isAssignableFrom( clazz ) ) {
            return JsonNodeType.NUMBER;
        }
        if ( Boolean.class.isAssignableFrom( clazz ) ) {
            return JsonNodeType.BOOLEAN;
        }
        return JsonNodeType.OBJECT;
    }
}
