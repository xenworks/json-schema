import com.github.spotbugs.SpotBugsTask
import com.xenoterracide.gradle.plugin.configurations.SpotBugs

plugins {
    id("com.xenoterracide.gradle.java-lib").version("0.4.7")
}

group = "com.xenoterracide"
description = "JSON Schema"

// In this section you declare the dependencies for your production and test code
dependencies {
    implementation("org.hibernate.javax.persistence:hibernate-jpa-2.1-api:1.+")
    implementation("javax.validation:validation-api")
    implementation("org.springframework:spring-core")
    implementation("com.fasterxml.jackson.core:jackson-annotations")
    implementation("com.fasterxml.jackson.core:jackson-databind")


    testImplementation("org.hamcrest:hamcrest-library")
    testImplementation("com.jayway.jsonpath:json-path")
    testImplementation("com.jayway.jsonpath:json-path-assert")
}
